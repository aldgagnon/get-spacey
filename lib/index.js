'use strict';

// Adds a space between each character in a string
const spacey = function(str) {
  return str
    .replace(/\s/g, '')
    .replace(/./g, '$& ')
    .trim()
    .toUpperCase();
};

module.exports = spacey;
