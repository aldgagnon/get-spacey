const assert = require('assert');
const spaceStrings = require('../index.js');

// Array of test data representing input and expected output
const testObjects = [
  {
    received: 'lorem',
    expected: 'L O R E M'
  },
  {
    received: 'L O R E M',
    expected: 'L O R E M'
  },
  {
    received: 'lorem ipsum',
    expected: 'L O R E M I P S U M'
  },
  {
    received: 'lorem1234567890',
    expected: 'L O R E M 1 2 3 4 5 6 7 8 9 0'
  },
  {
    received: '!@#$%^&*()_+-={}[]|\\:";\'<>?,./`~',
    expected: '! @ # $ % ^ & * ( ) _ + - = { } [ ] | \\ : " ; \' < > ? , . / ` ~'
  }
];

describe('get-spacey', () => {
  it('should properly format the array of test objects', () => {
    testObjects.forEach(function(object) {
      assert.equal(spaceStrings(object.received), object.expected);
    });
  });
});
