# get-spacey [![NPM version][npm-image]][npm-url] [![pipeline status](https://gitlab.com/aldgagnon/get-spacey/badges/master/pipeline.svg)](https://gitlab.com/aldgagnon/get-spacey/commits/master)
> Inserts a space between each character in a string. Inspired by Erik "Burnton" Burton.

## Installation

```sh
$ npm install --save get-spacey
```

## Usage

```js
const get-spacey = require('get-spacey');

get-spacey('Burnton');
//=> 'B U R N T O N'

```
## License

Apache-2.0 © [Andrew Gagnon]()


[npm-image]: https://badge.fury.io/js/get-spacey.svg
[npm-url]: https://npmjs.org/package/get-spacey
[travis-image]: https://travis-ci.org/aldgagnon/get-spacey.svg?branch=master
[travis-url]: https://travis-ci.org/aldgagnon/get-spacey
[daviddm-image]: https://david-dm.org/aldgagnon/get-spacey.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/aldgagnon/get-spacey
